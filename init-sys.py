#! /usr/bin/env python3
"""
A simple script to initialize the various directories for processing a new
system. The basic approach to managing ROMs that most of this scripts assume
is that they live in a directory hierarchy like this:
```
rom_managemnet/
|--dest/
   |--${system}/
   |--...more systems...
|--source/
   |--${system}/
   |--...more systems...
|--dats/
   |--${system}/
   |--...more systems...
|--scripts/
   |--...scripts are here..
```

This script is designed to just initialize the directories for a new system
and optionally add a configuration to JRomManager to point to those new
directories

Copyright © 2023 Tevis Money

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
# pylint: disable=invalid-name
#   Kebab case script names are more common in batocera

import argparse
import subprocess
import os
import sys
from pathlib import Path

SCRIPT_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))

DEST = Path(f"{SCRIPT_DIR}/../dest/").resolve()
SOURCE = Path(f"{SCRIPT_DIR}/../source/").resolve()
DATS = Path(f"{SCRIPT_DIR}/../dats").resolve()
JROM_MANAGER = "/Applications/JRomManager"

parser = argparse.ArgumentParser(
    description="Initialize the dirs for a new system")
parser.add_argument(
    "system", help="The system name to init (use batocera rom dir names)")
parser.add_argument("--init-properties", action="store_true",
                    help="Create a properties file for JRomManager")
parser.add_argument(
    "--jrm", help=f"The path to JRomManager, defaults to {JROM_MANAGER}")

parser.add_argument(
    "--dest",
    default=DEST,
    help=f"The directory to put the system output directory, defaults to {DEST}"
)
parser.add_argument(
    "--source",
    default=SOURCE,
    help=f"The directory to put the system input directory, defaults to {SOURCE}"
)
parser.add_argument(
    "--dats",
    default=DATS,
    help=f"The directory to put the system dats directory, defaults to {DATS}"
)

args = parser.parse_args()

if args.jrm:
    JROM_MANAGER = f"{args.jrm}"

DEST = f"{args.DEST}/{args.system}"
SOURCE = f"{args.SOURCE}/{args.system}"
DATS = f"{args.DATS}/{args.system}"

PROPS_TEXT = """\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
<entry key="backup">false</entry>
<entry key="use_parallelism">true</entry>
<entry key="filter.YearMax">????</entry>
<entry key="src_dir">{source}</entry>
<entry key="roms_dest_dir">{dest}</entry>
<entry key="format">ZIP</entry>
<entry key="filter.YearMin"></entry>
<entry key="automation.scan">SCAN_REPORT</entry>
<entry key="hash_collision_mode">SINGLEFILE</entry>
<entry key="merge_mode">SPLIT</entry>
</properties>
"""

subprocess.run(["mkdir", "-p", DEST, SOURCE, DATS], check=True)
if args.init_properties:
    JROM_MANAGER = f"{JROM_MANAGER}/presets/{args.system}.properties"
    with open(JROM_MANAGER, 'w', encoding="utf-8") as file:
        file.write(PROPS_TEXT.format(source=os.path.abspath(
            SOURCE), dest=os.path.abspath(DEST)))
