# ROM Management Scripts #

The basic approach to managing ROMs that most of this scripts assume
is that they live in a directory hierarchy like this:

```plain-text
rom_managemnet/
|--dest/
   |--${system}/
   |--...more systems...
|--source/
   |--${system}/
   |--...more systems...
|--dats/
   |--${system}/
   |--...more systems...
|--scripts/
   |--...scripts are here..
```

The scripts are designed to largely help support a workflow that takes
source ROMs from a `source/${system}` directory, use DAT files in
`dats/${system}` to pare them down and output the final collection to
`dest/${system}`. From there that output is designed to be rsynced to the
Batocera system for use.

Most of the scripts should have options to allow them to be used from and to
any destination and not just this hierarchy but this is the default assumption.

## Main Suport Scripts ##

These are found in the top level of this directory and are intended to be basic
support for the workflow described above. Mostly initializing new system dirs
and helpers for moving the files around.

## Batocera Scripts ##

Scripts in the `batocera` directory are designed to be run on the Batocera box
and manipulate files there directly. Copy them to someplace like
`/userdata/sys/bin` and run them from there. To run them you may need to give
them the executable permission with `chmod +x ${file}`.

⚠️ These scripts can be descructive and you should read them over an understand
what they are doing before running them.

## Apple ][ Scripts ##

The script and support files in the `apple2` directory are to help convert the
[4am Apple 2 collection][4am-crack] of files to something suitable for use
with Batocera. Additionally because the 4am collection is built with full
archival in mind, a lot of the files aren't games and not necessarily something
you want in your Batocera collection, so the script is also designed to help
pare those down as well.

This script does not work with the default directory structure described above
so be sure to check out the README.md in the directory for more details.

## C64 Dreams Scripts ##

The scripts in the `c64-dreams` directory are designed to help process the
C64 Dreams Collection[c64-dreams] into a something suitable for use with
Batocera. Unlike the apple2 scripts, since the C64 Dreams collection is curated
there aren't options to pare the collection down. There are some options to
control how the final collection's media is created to be sure to check out the
help documentation. These scripts can work with the directory structure
described above, but by default output is next to the script itself.

[4am-crack]: https://archive.org/details/apple_ii_library_4am
[c64-dreams]: https://www.zombs-lair.com/blog/categories/c64-dreams
