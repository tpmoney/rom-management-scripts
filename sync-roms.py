#! /usr/bin/env python3
"""
Basic script for rsyncing a set of ROMs for a given system
to Batocera. Doesn't do anything fancy, just really to record what args I
usually use when doing this.

Copyright © 2023 Tevis Money

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
# pylint: disable=invalid-name
#   Kebab case script names are more common in batocera

import argparse
import os
import subprocess
import sys
from pathlib import Path

SCRIPT_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))
DEFAULT_SYNC_DIR = Path(f"{SCRIPT_DIR}/../dest/").resolve()


def main(args):
    """
    Main function to rsync the roms dir to Batocera
    """
    # pylint: disable=redefined-outer-name
    if args.roms:
        sync_dir = args.roms
    else:
        sync_dir = Path(f"{DEFAULT_SYNC_DIR}/{args.system}/").resolve()

    command = [
        "rsync",
        "-av",
        "--progress",
        "--include", "*.zip",
        "--exclude", "*",
        "--delete"
    ]

    if args.dry_run:
        command.append("--dry-run")

    command.append(sync_dir)
    command.append(f"root@{args.host}:/userdata/roms/{args.system}")

    subprocess.run(command, check=True)


def parse_args():
    """
    Parse the command line arguments

    ### Return ###
    The parsed set of arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("system", help="The system to sync")
    parser.add_argument("--dry-run", action="store_true",
                        help="Don't actuall sync, just show what would happen")
    parser.add_argument(
        "--roms", help=f"The location of the roms dir defaults to {DEFAULT_SYNC_DIR}/${{system}}")
    parser.add_argument(
        "-H", "--host", help="Batocera host name", default="batocera")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    main(args)
