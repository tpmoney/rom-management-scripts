#! /usr/bin/env python3
"""
################################################################################
# Extract disks from the 4am Apple II crack collection
# (https://archive.org/details/apple_ii_library_4am) based on genre
#
# Note that this is destructive to the directory called "extracted"
################################################################################

Copyright © 2023 Tevis Money

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
# pylint: disable=invalid-name
#   Kebab case script names are more common in batocera

import argparse
import copy
import os
import re
from os import PathLike
import shutil
import sys
import tempfile

# File paths
genres_file = "./genres.txt"
overrides_file = "./override-copy.txt"
roms_dir = "./4am"
extraction_dir = "./extracted"

# Global args
args = None


class Rom:
    """
    Defines an object to represent a ROM to potentially be copied
    """

    def __init__(self, name=None, paths=None, version=None,
                 genre=None, publisher=None, forced=False):
        """
        Create a new ROM object

        ### Arguments ###
        * `name` - The Name of this ROM
        * `paths` - Paths to the ROM archive or disk images
        * `version` - The version number of this ROM
        * `genre` - The genre of this ROM
        * `publisher` - The publisher of this ROM
        * `forced` - Whether or not this ROM was force included by the user
        """
        self.name = name
        if paths:
            self.paths = list(paths)
        else:
            self.paths = list()
        self.version = version
        self.genre = genre
        self.publisher = publisher
        self.forced = forced

    def add_path(self, path: os.PathLike):
        """
        Add a path to the set of files to copy for this ROM

        ### Arguments ###
        * `path` - The path to add
        """
        self.paths.append(path)

    def add_paths(self, paths: list[os.PathLike]):
        """
        Add multiple paths to the set of files to copy for this ROM

        ### Arguments ###
        * `paths` - A list of PathLike objects
        """
        self.paths.extend(paths)


def main():
    """
    Main function for the script
    """

    clear_extracted_dir()

    # Roms to evaluate and filter
    roms = walk_dir(roms_dir)
    roms = filter_forced(roms)
    roms = filter_genres(roms, args.ignore_genres)
    roms = filter_old_versions(roms)
    copy_files(roms, args.unzip)
    if not args.preserve_work_disks:
        clean_work_disks(extraction_dir)
    print_hints()


def clear_extracted_dir():
    """
    Clear the extracted directory of all current files
    """
    for root, dirs, files in os.walk(extraction_dir):
        for file in files:
            os.unlink(os.path.join(root, file))
        for directory in dirs:
            shutil.rmtree(os.path.join(root, directory))


def walk_dir(path: PathLike) -> list[Rom]:
    """
    Recursively walk a directory tree looking for roms

    ### Arguments ###
    * `path` - The path to recursively walk

    ### Return ###
    The list of Roms parsed from the directory
    """

    roms = list()
    rom = Rom()
    for item in os.scandir(path):
        if item.is_file():
            rom = handle_file(item, rom)
        if item.is_dir():
            roms.extend(walk_dir(item))

    if rom.name:
        roms.append(rom)

    return roms


def handle_file(file: os.DirEntry, rom: Rom) -> Rom:
    """
    When encountering a file in a directory walk, evaluate it to see if it's a
    `.txt` file with a genere defined in it. If it is, read the metadata into
    the ROM object.

    Otherwise if it's a disk image, add it to the files to copy.

    Otherwise if it's a zip file with a name ending in `crack).zip`, add it to
    the files to copy.

    Otherwise ignore the file

    ### Arguments ###
    * `file` - The file name
    * `rom` - The ROM this file is going to be for

    ### Return ###
    The modified Rom oject
    """
    _, ext = os.path.splitext(file.name)
    if ext == ".txt":
        read_metadata_into_rom(file, rom)
    elif ext in (".dsk", ".gm2") and not file.name.startswith("00playable"):
        rom.add_path(file)
    elif file.name.endswith("crack).zip"):
        rom.add_path(file)

    return rom


def read_metadata_into_rom(file: os.DirEntry, rom: Rom):
    """
    Read the metadata out of a rom txt file and insert it into the ROM object

    ### Arguments ###
    * `file` - The txt file to read metadata from
    * `rom` - The ROM object to add the metadata to
    """
    with open(file.path, encoding="utf-8") as f:
        reading_name = False
        for idx, line in enumerate(f):
            if idx > 20:
                break
            meta_match = re.match(
                r"(Name|Genre|Version|Publisher):\s+(.*)", line)
            if meta_match:
                label = meta_match.group(1)
                value = meta_match.group(2)
                if label == "Name":
                    rom.name = value
                    reading_name = True
                else:
                    if label == "Genre":
                        rom.genre = value
                    elif label == "Version":
                        rom.version = value
                    elif label == "Publisher":
                        rom.publisher = value
                    reading_name = False
            elif reading_name:
                name_continue_match = re.match(r"^\s(.*)$", line)
                if name_continue_match:
                    rom.name = rom.name + name_continue_match.group(1)
                else:
                    reading_name = False


def filter_forced(roms: list[Rom]) -> list[Rom]:
    """
    Mark all the ROM files that should be force included, and remove ones that
    were force excluded

    ### Arguments ###
    * `roms` - The list of ROMs to filter

    ### Return ###
    The filtered list of Roms
    """
    sys.stderr.write("Filtering roms from user overrides...\n")
    force_include, force_exclude = load_overrides()

    def filter_fun(rom: Rom):
        if rom.name in force_exclude:
            return False
        if rom.name in force_include:
            rom.forced = True
        return True

    return list(filter(filter_fun, roms))


def load_overrides():
    """
    Load the user's overrides from the overrides file

    ### Return ###
    A tuple containing:
    * `force_include` - The set of ROM names to include
    * `force_exclude` - The set of ROM names to exclude
    """
    force_include = set()
    force_exclude = set()
    if os.path.exists(overrides_file):
        with open(overrides_file, encoding="utf-8") as f:
            for line in f:
                m = re.match(r"([+-])\s*(.*)", line)
                if m:
                    if m.group(1) == "+":
                        force_include.add(m.group(2))
                    else:
                        force_exclude.add(m.group(2))
    return (force_include, force_exclude)


def filter_genres(roms: list[Rom], ignore_genres_file: bool) -> list[Rom]:
    """
    Filter the ROMs to remove ones in the genres that we don't want.

    Additionally prompts the user to save the genere filters for future use

    ### Arguments ###
    * `roms` - The list of ROMs to filter
    * `ignore_genres_file` - If True, don't load from the genres file and
                             don't write a new one at the end

    ### Return ###
    The filtered list of ROMs
    """
    sys.stderr.write("Filtering ROMs by genre...\n")

    include_genres, exclude_generes = load_genres(ignore_genres_file)

    def filter_fun(rom: Rom):
        if rom.forced:
            return True
        elif rom.genre in exclude_generes:
            return False
        elif rom.genre in include_genres:
            return True
        else:
            if include_genre_ask(rom.genre):
                include_genres.add(rom.genre)
                return True
            else:
                exclude_generes.add(rom.genre)
                return False

    if not ignore_genres_file:
        save_genres(include_genres, exclude_generes)

    return list(filter(filter_fun, roms))


def save_genres(include: list[str], exclude: list[str]):
    """
    Prompt the user and write out the specified set of genres to the genre file
    """
    # if os.path.exists(genres_file) and \
    #         input("Overwrite existing genres.txt file? [y/N]: ") not in ("y", "Y"):
    #     return

    with open(genres_file, 'w', encoding="utf-8") as f:
        for genre in include:
            f.write("+ " + genre + "\n")
        for genre in exclude:
            f.write("- " + genre + "\n")


def load_genres(ignore_genres_file: bool):
    """
    Load the genres file to save on further user prompting

    ### Arguments ###
    * `ignore_genres_file` - If `True` don't load the user genres file

    ### Return ###
    A tuple containing:
    * `include_genres` - A set of genres to include
    * `exclude_genres` - A set of genres to exclude
    """
    include_genres = set()
    exclude_genres = set()
    if os.path.exists(genres_file) and not ignore_genres_file:
        with open(genres_file, encoding="utf-8") as f:
            for line in f:
                m = re.match(r"([+-])\s*(.*)", line)
                if m:
                    if m.group(1) == "+":
                        include_genres.add(m.group(2))
                    else:
                        exclude_genres.add(m.group(2))
    return (include_genres, exclude_genres)


def include_genre_ask(genre: str):
    """
    Prompt the user for determination if this genre should or should not be
    included

    ### Arguments ###
    * `genre` - The genre to consider

    ### Return ###
    `True` if the genre is one the user would like to include
    """
    include = input("Include items in genre \"" + genre + "\" [y/N]: ")
    return include in ("y", "Y")


def filter_old_versions(roms: list[Rom]) -> list[Rom]:
    """
    Filter the roms by version to preserve only the newest versions

    ### Arguments ###
    * `roms` - The list of ROMs to filter

    ### Return ###
    The filtered list of ROMs
    """

    sys.stderr.write("Filtering versions...\n")
    latest_version = dict()
    for rom in roms:
        lv = latest_version.get(rom.name)
        if not lv or (rom.version is not None and lv < rom.version):
            latest_version[rom.name] = rom.version

    def filter_fun(rom):
        if rom.forced:
            return True
        elif rom.version == latest_version[rom.name]:
            return True
        else:
            return False

    return list(filter(filter_fun, roms))


def copy_files(roms: list[Rom], unzip: bool):
    """
    Copy all the files in each Rom's `files_to_copy` to the extraction directory

    ### Arguments ###
    * `roms` - The list of ROMs to copy
    * `unzip` - Whether or not to unzip the ROM archives or leave them compressed
    """
    for rom in roms:
        if len(rom.paths) == 1:
            # This is either an archive or single disk ROM
            if unzip and rom.paths[0].name.endswith(".zip"):
                unzip_rom_archive(rom)
            else:
                shutil.copy(rom.paths[0], extraction_dir)
        else:
            # This is a set of disk images and need a folder
            copy_non_archive_rom(rom)

    print(f"Copied {len(roms)} roms")


def unzip_rom_archive(rom: Rom):
    """
    Unzip an archive into the extraction directory, if the archive didn't
    contain a proper containing directory make one
    """
    with tempfile.TemporaryDirectory() as tempdir:
        shutil.unpack_archive(rom.paths[0], tempdir)
        files = os.listdir(tempdir)
        if len(files) == 1:
            try:
                shutil.move(os.path.join(tempdir, files[0]), extraction_dir)
            except shutil.Error:
                sys.stderr.write(
                    files[0] + " is already in the extraction dir, skipping!\n")
        else:
            multi_file_temp_rom = copy.copy(rom)
            multi_file_temp_rom.paths = list()
            for file in files:
                if file.endswith(".dsk") or file.endswith(".gm2"):
                    multi_file_temp_rom.add_path(os.path.join(tempdir, file))
            copy_non_archive_rom(multi_file_temp_rom)


def copy_non_archive_rom(rom: Rom):
    """
    Copy all the files in a non archival ROM to the extraction directory
    """
    dir_name = rom.name

    publisher = False
    version = False
    while True:
        try:
            dir_name = dir_name.replace("/", "_") \
                .replace("'", "")
            new_dir = os.path.join(extraction_dir, dir_name)
            os.mkdir(new_dir)
            break
        except OSError as err:
            if not publisher and rom.publisher:
                publisher = True
                dir_name = dir_name + " - " + rom.publisher
            elif not version and rom.version:
                version = True
                dir_name = dir_name + " - " + rom.version
            else:
                sys.stderr.write(err.strerror)
                sys.stderr.write(f"\nTried to make multi copy dir {new_dir} \
                                 for {rom.name} but it already exists. Skipping.\n")
                return
    for file in rom.paths:
        shutil.copy(file, new_dir)


def clean_work_disks(directory: os.PathLike):
    """
    Remove the "work disk" disks from a directory. For Batocera we really don't
    need these, but they can be used for debugging issues with a cracked rom

    ### Arguments ###
    * `directory` - The directory to clean
    """
    for root, _, files in os.walk(directory):
        for file in files:
            if "work disk" in file:
                os.unlink(os.path.join(root, file))


def print_hints():
    """
    Print out user friendly hints about what to do with the extracted files
    """
    print("""
    Now that all the file have been extracted, you can send them to your system
    by running the following command:

    rsync -rv --progress --delete \\
        --include "*/" \\
        --include "*.zip" \\
        --include "*.dsk" \\
        --include="*.gm2" \\
        --exclude="*"  \\
        ./extracted/ root@batocera:/userdata/roms/apple2gs/
    """)


HELP_TEXT = """
Parse the 4am Collection of Apple II software and filter it per user choices.

A preset list of chosen genres can be loaded to avoid prompting the user by
defining a file `genres.txt` with the following format:
```
# genres.txt file
# Genres to include
+ adventure
+ action
# Genres to exclude
- reference
- education
```

Additionally you can force the inclusion of roms that would otherwise be
included or excluded by genre filtering by defining a `override-copy.txt` file
with the names of the ROMs you want to include or exclude in the following
format:
```
# override-copy.txt file
# ROMs to force include
+ Word Munchers
+ The Oregon Trail
# ROMs to exclude
- Space Invaders
```
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=HELP_TEXT)
    parser.add_argument("-u", "--unzip", action="store_true",
                        help="Unzip the archives instead of extracting them directly")
    parser.add_argument("-p", "--preserve-work-disks", action="store_true",
                        help="Preserve the 'work disk' disks from 4am")
    parser.add_argument("--ignore-genres", action="store_true",
                        help="Ignore the genres.txt file if present")

    args = parser.parse_args()
    main()
