The scripts in this folder were designed to extract ROM files from
the 4am crack collection. Place them in a top level directory next
to the 4am collection in a dir called `4am`. Make another dir
called `extracted` and run the `game-extractor.py` script. It will
follow the rules in the script and extract the games out to the
`extracted` dir where they can be synced up as per usual
