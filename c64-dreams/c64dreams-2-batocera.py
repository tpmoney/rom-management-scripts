#! /usr/bin/env python3
"""
Script that will attempt to convert a C64 Dreams collection into a
folder structure suitable for copy to Batocera.

To find special cases where we're doing something to handle a specific game
that isn't handled in a any of the generic processes, look for the
## SPECIAL HANDLING ## comments

Copyright © 2023 Tevis Money

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
# pylint: disable=invalid-name
#   Kebab case script names are more common in batocera
# pylint: disable=too-many-lines
#   Want to keep this as a single script with no dependencies, unfortunately
#   this isn't a simple process, so lots of lines

from __future__ import annotations

import argparse
from bisect import bisect_left
from datetime import datetime
from enum import Enum
import itertools
import logging
from os import PathLike
import os
import platform
import re
import shutil
import subprocess
import textwrap
import tomllib
from typing import Callable
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement
from pathlib import Path

DESCRIPTION = """\
A simple script to convert a C64 Dreams collection into a folder structure
suitable for copy to Batocera. Last tested on the v60 collection. Goals are
to produce:
  - a roms directory, with proper subdirectories
  - playlists for multi disk games
  - a gamelist.xml file with appropriate metadata
  - an images directory with appropriate game images
"""

CONFIG_FILES = textwrap.dedent("""
This script will search for a TOML file called `c64dreams2batocera.ini` in the
user's ${HOME}/.config directory. This file can contain two sections `parse`
and `rsync` and will be used to configure the default behavior of the script.
Any optional arguments can be specified in this file. Arguments with a `-` in
the name should instead be written in snake_case. For optional arguments that
enable an action (e.g. the `--clean` option), specify them with a `true` or
`false` value. Any argument in the config file can be overridden by passing the
same argument in the command line. For arguments that enable an action, specify
the equivalent `no-action` argument on the command line to disable the action.

### Example Config ###
The following is an example config file that sets the following arguments for
parse and rsync modes:
`parse --clean --isolate-roms=c64_dreams`
`rsync --delete --keep-temp`

```
[parse]
clean = true
isolate-roms = "c64_dreams"

[rsync]
delete = true
keep-temp = true
```

If using the above config, you wanted to not clean the output directory when
parsing, you would pass `--no-clean` on the command line to override the `clean`
directive in the config.
""")

KNOWN_ISSUES = textwrap.dedent("""
* RetroBat and `.d81` images - While this script should be suitable for any
Emulation Station based system, not all systems ship with an emulator that
handles the `d81` format. A user on the C64 Dreams discord reports that RetroBat
is one such system and that `d81` games do not appear in the game list when
using this script.
""")

SPECIAL_THANKS = textwrap.dedent("""
* Zombeaver (https://www.zombs-lair.com) - As the author and compiler of the C64
Dreams collection, none of this would be possible without the many hours of work
they have put into curating the collection.

* The Batocera Linux Team (https://batocera.org) - Their efforts have put
together an incredible retro gaming multi system with excellent defaults and
support for a number of different systems and hardware.

* Boris Schneider-Johne - A user from the C64 Dreams Discord server who has
provided invaluable feedback and testing of this script in Windows environments.

And lastly all the C64 developers and authors through the years who have made
so many incredible games and memories for us all, and have inspired so many to
work so dilligently to keep the memories alive for everyone.
""")

# C64 Dreams Screenshot directory names
C64D_SCREENSHOT_GAMEPLAY_DIR = "Screenshot - Gameplay"
C64D_SCREENSHOT_TITLE_DIR = "Screenshot - Game Title"
C64D_SCREENSHOT_SELECT_DIR = "Screenshot - Game Select"
C64D_SCREENSHOT_GAME_OVER_DIR = "Screenshot - Game Over"
C64D_SCREENSHOT_HIGH_SCORES_DIR = "Screenshot - High Scores"

# C64 Dreams Marquee directory names
C64D_CLEAR_LOGO_DIR = "Clear Logo"
C64D_BANNER_DIR = "Banner"
C64D_ARCADE_MARQUEE_DIR = "Arcade - Marquee"

# C64 Dreams box front directory names
C64D_3D_BOX_DIR = "Box - 3D"
C64D_BOX_FRONT_DIR = "Box - Front"
C64D_BOX_FRONT_RECONSTRUCTED_DIR = "Box - Front - Reconstructed"
C64D_BOX_FRONT_FANART_DIR = "Fanart - Box - Front"
C64D_DISK_FANART_DIR = "Fanart - Disk"
C64D_CART_FRONT_DIR = "Cart - Front"

# C64 Dreams box back directory names
C64D_BOX_BACK_DIR = "Box - Back"
C64D_BOX_BACK_RECONSTRUCTED_DIR = "Box - Back - Reconstructed"
C64D_BOX_BACK_FANART_DIR = "Fanart - Box - Back"
C64D_CART_BACK_DIR = "Cart - Back"

ARGS = None
DEFAULT_OUT_DIR = "./c64dreams"

IMAGE_FORMATS = (".d64", ".D64", ".crt", ".d81", ".t64", ".g64", ".tcrt")

LOG = logging.getLogger(__name__)
DETAIL = 15  # Logging level for "detailed" logs, in between DEBUG and INFO
GAME_TRACE = 55  # Logging level for game traces, highest level to alwasy log

# Images in the C64 Dreams collection have certain special characters
# replaced in their game names because they're reserved in Windows
WINDOWS_SAFE_REPLACEMENTS = str.maketrans({
    "'": "_",
    ":": "_",
    "?": "_",
    "/": "_",
    "*": "_",
})

WINDOWS_SAFE_ALTERNATES = str.maketrans({
    "'": "_",
    ":": " -",
    "?": "_",
    "/": "_",
    "*": "_",
})

IMAGE_DIR_CACHE = dict()

SPINNER = itertools.cycle(["|", "/", "-", "\\"])

### Code for parsing the C64 Dreams Collection ###


def parse():
    """
    Parse the C64 Dreams Collection and export the gamelist and roms
    """
    out_dir = Path(ARGS.out_dir).expanduser().resolve()
    source_dir = Path(ARGS.source).expanduser().resolve()

    temp_dir = init_destination(out_dir, ARGS.clean)

    lb_source_list = f"{source_dir}/Data/Platforms/Games.xml"
    lb_images_dir = f"{source_dir}/Images/C64 Dreams/"
    # lb_videos_dir = f"{source_dir}/Videos/C64 Dreams/"

    games_dict = parse_source_list(lb_source_list)
    find_roms(games_dict, source_dir, temp_dir)
    find_manuals(games_dict, source_dir)
    # find_videos(games_dict, lb_videos_dir)
    LOG.info(
        "Skipping Videos because they are not distributed with the C64 Dreams collection")
    find_images(games_dict, lb_images_dir, ARGS.fast_images,
                Box3DPreference.parse_arg(ARGS.box_3d), ARGS.allow_fanart_boxes)
    move_files(games_dict, out_dir)
    export_gamelist(games_dict, out_dir)
    if ARGS.copy_files:
        clean_up_temp_dir(temp_dir)

    LOG.info("Converted %i games", len(games_dict))
    LOG.info("Done")


def init_destination(out_dir: PathLike, clean: bool) -> Path:
    """
    Create the images and manuals directories in the output directory

    ### Arguments ###
    * `out_dir` - The primary output directory
    * `clean` - Whether or not to clean all the existing files and dirs from
                the output directory

    ### Returns ###
    The path to the temp_dir for use during the parsing process
    """

    if clean:
        for root, dirs, files in os.walk(out_dir, topdown=False):
            for file in files:
                os.remove(os.path.join(root, file))
            for directory in dirs:
                os.rmdir(os.path.join(root, directory))

    images_dir = f"{out_dir}/images"
    manuals_dir = f"{out_dir}/manuals"
    temp_dir = f"{out_dir}/temp"

    LOG.debug("""
    images_dir: %s
    manuals_dir: %s
    temp_dir: %s
    """, images_dir, manuals_dir, temp_dir)

    os.makedirs(images_dir, exist_ok=True)
    os.makedirs(manuals_dir, exist_ok=True)
    os.makedirs(temp_dir, exist_ok=True)

    return temp_dir


def parse_source_list(lb_source_list):
    """
    Parse the launchbox gamelist insto a dictionary of Game objects
    for further processing
    """
    LOG.info("Parsing launchbox gamelist")
    games_dict = {}
    for game in ElementTree.parse(lb_source_list).findall("Game"):
        games_dict[game.find("ID").text] = Game.from_launchbox_game(game)

    LOG.info("Parsed %i games from launchbox gamelist", len(games_dict))
    # TODO: Handle AdditionalApplication's where they are game versions
    return games_dict


def find_roms(games_dict, source_dir: PathLike, temp_dir: PathLike):
    """
    Copy or symlink the roms from the source directory to the destination
    """

    LOG.info("Locating ROMs for games")
    commercial_games = dict()
    special_handling_required = dict()
    for game in games_dict.values():
        spinner()
        debug_game_log(game.name, "Locating ROMs for: %s", game.name)
        try:
            determine_rom_files(game, source_dir, temp_dir)
        except CommercialGameError:
            commercial_games[game.game_id] = game.name
            continue
        except SpecialHandlingError as err:
            special_handling_required[game.game_id] = (game, err.reason)
            continue

    # Print out any games we skipped for being commercial games
    for key, val in commercial_games.items():
        games_dict.pop(key)

        if val == ARGS.game_trace:
            log_level = GAME_TRACE
        else:
            log_level = DETAIL
        LOG.log(log_level, "Will skip: %s because it was a commercial game", val)

    # Print out any games we skipped for needing special handling
    for key, (rom, reason) in special_handling_required.items():
        games_dict.pop(key)

        if rom.name == ARGS.game_trace:
            log_level = GAME_TRACE
        else:
            log_level = DETAIL
        LOG.log(log_level,
                "Will skip: %s because it was a special handling game: %s", rom.name, reason)

    LOG.info("Stats: %i games, %i commercial, %i special handling needed",
             len(games_dict), len(commercial_games), len(special_handling_required))


def determine_rom_files(game: Game, source_dir: PathLike, temp_dir: PathLike):
    """
    Determine the primary and secondary files for a game
    """
    primary_file = None
    secondary_files = list()
    vbs_file = Path(source_dir, game.path.replace("\\", "/"))
    rom_dir = vbs_file.parent
    manual_m3u = False  # Flag to indicate we need to make an m3u file

    game.rom_dir = rom_dir

    ## SPECIAL HANDLING ##
    # Known Special Handling Cases:
    # TODO: Reported to Zombeaver and corrected
    # This should probably be unnecessary in versions after v0.60
    if rom_dir.name == "H.E.R.O.":
        rom_dir = Path(rom_dir.parent, "H.E.R.O")
        game.rom_dir = rom_dir
    ## END SPECIAL HANDLING ##

    # First try to parse out from the VBS file
    try:
        (primary_file, prog_name) = extract_primary_rom_from_vbs(vbs_file)
        if primary_file.suffix == ".m3u":
            with open(primary_file, "r", encoding="utf-8") as file:
                for name in file.readlines():
                    name = name.strip()
                    matcher = re.match(r"([^\:]+)\:[^\:]+$", name)
                    if matcher:
                        name = matcher.group(1)
                    secondary_file = Path(rom_dir, name)
                    if secondary_file.exists():
                        secondary_files.append(secondary_file)
                    else:
                        ### Special Handling ###
                        # TODO: Issue reported to Zombeaver, corrected and this
                        # should probably be unnecessary in versions after v0.60
                        if game.name in [
                                "Ultima II: The Revenge of the Enchantress",
                                "Energy Manager"]:
                            LOG.log(DETAIL,
                                    "%s is missing secondary file %s. This is expected",
                                    game.name,
                                    secondary_file)
                            continue
                        ### END SPECIAL HANDLING ###

                        raise SpecialHandlingError(
                            f"Missing specified secondary file {secondary_file}")
        elif prog_name:
            # The cmd specified a program name, so generate a custom m3u
            secondary_files.insert(0, primary_file)
            primary_file = create_temp_m3u(
                game.name, secondary_files, temp_dir, prog_name)

        game.add_file(primary_file)
        for secondary_file in secondary_files:
            game.add_file(secondary_file)

        debug_game_log(game.name,
                       """
                       Found ROM files from VBS file:\n
                       \tM3U: %s\n
                       \tROMS: %s
                        """,
                       primary_file, secondary_files)
        return
    except FileNotFoundError:
        # Could extract it, let's try to guess
        pass
    except NeedsManualM3UError:
        # Try to create a custome m3u file
        manual_m3u = True

    # Read in the files
    for file in rom_dir.iterdir():
        if file.is_file() and file.suffix in IMAGE_FORMATS:
            secondary_files.append(file)
        elif file.is_file() and file.suffix == ".m3u":
            primary_file = file

    if primary_file and len(secondary_files) == 0:
        # We have an m3u file but no images
        raise FileNotFoundError(
            f"No secondary ROM image files found in: {rom_dir}")
    elif not primary_file and len(secondary_files) > 1:
        if manual_m3u:

            debug_game_log(
                game.name, "Creating manual m3u file for game: %s", game.name)

            secondary_files = sort_secondary_files(secondary_files)
            primary_file = create_temp_m3u(
                game.name, secondary_files, temp_dir)
        elif len(secondary_files) == 2 and \
                any("Alt" in file.name for file in secondary_files):
            debug_game_log(
                game.name, "Skipping 'Alt' secondary file", game.name)
            # Set the primary file to the secondary file without "Alt" in the name
            primary_file = [
                file for file in secondary_files if not "Alt" in file.name][0]
            secondary_files = ()
        else:
            raise SpecialHandlingError(
                f"More than one secondary ROM image file found in: {rom_dir}"
            )
    elif not primary_file:
        # No m3u, only one image, that's our primary
        primary_file = secondary_files[0]
        secondary_files = list()
    else:
        # m3u file so let's get the roms
        try:
            with open(primary_file, "r", encoding="utf-8") as file:
                roms = file.read().splitlines()
                # Remove trailing `:foo` on entries
                roms = [re.match(r"([^:]+)(?::.+)?", rom).group(1)
                        for rom in roms]
        except UnicodeDecodeError as err:
            raise RuntimeError(f"Error reading {primary_file}") from err
        secondary_files = [
            file for file in secondary_files if file.name in roms
        ]

        if len(secondary_files) != len(roms):

            raise AssertionError(
                f"""
                Not all m3u files found in rom images
                Images: {[item.name for item in secondary_files]}
                m3u List: {roms}
                """)

    game.add_file(primary_file)
    for file in secondary_files:
        game.add_file(file)

    debug_game_log(game.name,
                   """
                   Found ROM files for in ROM dir:\n
                   \tPrimary File: %s\n
                   \tSecondary Files: %s
                   """,
                   primary_file, secondary_files)


def extract_primary_rom_from_vbs(vbs_file: Path) -> tuple(Path, str):
    """
    Try to extract the primary ROM by following the chain of launch
    files and commands from launchbox

    ### Arguments ###
    * `vbs_file` - The VBS file to parse

    ### Returns ###
    A tuple containing:
    - The primary ROM file
    - If specified in the cmd file, the name of the specific program to launch
    """

    # First try to extract a `.bat` file name from the `.vbs` file
    bat_file = extract_bat_file_from_vbs(vbs_file)

    # Now try to extract a `.cmd` file from the `.bat`
    bat_parse = parse_bat_file(bat_file)
    if bat_parse.suffix != ".cmd":
        return (bat_parse, None)

    (rom_file, prog_name) = extract_rom_from_cmd(bat_parse)

    if not rom_file.exists():
        raise FileNotFoundError(
            f"Could not find extracted ROM file {rom_file}")
    elif rom_file.suffix == ".m3u":
        raise AssertionError(
            f"Extracted primary rom file {rom_file} is a m3u file")
    return (rom_file, prog_name)


def extract_rom_from_cmd(cmd_file: PathLike) -> tuple[PathLike, str]:
    """
    Extract the rom name from a .cmd file

    ### Returns ###
    A tuple containing:
    - The path to the rom file extracted from the file
    - If specified in the cmd file, the name of the specific program to launch

    ### Raises ###
    `AssertionError` - if the cmd file has more than one line
    `SpecialHandlingError` - if the cmd file is a cart with a save disk
                             and needs a manual m3u file created
    """
    with open(cmd_file, "r", encoding="utf-8") as file:
        lines = file.read().splitlines()
        if len(lines) > 1:
            raise AssertionError(f"Multiple lines in cmd file {cmd_file}")
    cmd_line = lines[0].strip('"').strip().replace("\\", "/")
    prog = None

    if cmd_line.startswith("-cartcrt"):
        raise NeedsManualM3UError("Uses save disk, needs manual M3U")

    matcher = re.match(r"([^\:]+)\:([^\:]+$)", cmd_line)
    if matcher:
        cmd_line = matcher.group(1)
        prog = matcher.group(2)

    rom_file = Path(cmd_file.parent, cmd_line)
    return (rom_file, prog)


def extract_bat_file_from_vbs(vbs_file: PathLike):
    """
    Read the given VBS file and try to extract the name
    of the .bat file inside

    ### Returns ###
    The bat file name

    ### Raises ###
    `AssertionError` - if no .bat file is found
    """
    with open(vbs_file, "r", encoding="utf-8") as file:
        for line in file:
            match = re.match(r'WshShell[^"]+ "([^"]+\.bat)" .*', line)
            if match:
                bat_file = Path(vbs_file.parent, match.group(1))
                break
        if not bat_file:
            raise AssertionError(f"No .bat file found in {vbs_file}")
    return bat_file


def parse_bat_file(bat_file: PathLike):
    """
    Parse the bat file and either extract the cmd file or extract the
    actual rom file

    ### Returns ###
    The cmd file path, or the path to the actual rom file
    """
    directory = bat_file.parent
    cmd_file = None
    with open(bat_file, "r", encoding="utf-8") as file:
        # First read the .cmd file checking for either a
        # .cmd line, or checking if this points to a commercial
        # game, or if it might even point directly to a ROM file
        for line in file:
            match = re.match(r'.+"([^"]+\.cmd)"$', line)
            if match:
                cmd_file = Path(directory, match.group(1))
                break

            # If we didn't find a `.cmd` file this line, check whether
            # this points to the commercial game instructions
            if re.match(r'.+Commercial C64 Game.gif"$', line):
                # This is a commercial game still available
                # we don't handle these right now
                raise CommercialGameError

            # Sometimes the `.bat` just points direct to a rom image, so use
            # that instead
            match = re.match(r'.+"([^"]+\.(d81|crt|m3u))"$', line)
            if match:
                name = match.group(1).strip('"').replace("\\", "/")
                rom_file = Path(directory, name)
                if rom_file.name.startswith("%folder%"):
                    # Will deal with this below
                    cmd_file = rom_file
                elif not rom_file.exists():
                    raise FileNotFoundError(
                        f".bat file declared ROM, could not find it: {rom_file}")
                else:
                    return rom_file

        if cmd_file.name.startswith("%folder%"):
            cmd_file = Path(directory, f"{directory.name}{cmd_file.suffix}")
            if not cmd_file.exists():
                cmd_file = None

        if not cmd_file:
            # No CMD file was specifically declared
            # So now we walk through the options in
            # the usual bat files
            extensions = {".crt", ".d81", ".m3u"}
            for ext in extensions:
                rom_file = Path(directory, f"{directory.name}{ext}")
                if rom_file.exists():
                    return rom_file
            raise FileNotFoundError(
                f"Could not find cmd or rom files from {bat_file}")

        return cmd_file


def sort_secondary_files(secondary_files):
    """
    Try to sort the secondary files by disk order, and
    with the "Characters.d64" file last
    """
    sorted_secondary_files = list()
    save_disk = None
    for file in secondary_files:
        if file.name in ["Characters.d64", "Blank.d64"]:
            save_disk = file
        else:
            sorted_secondary_files.append(file)
    sorted_secondary_files.sort(key=lambda file: file.name)
    if save_disk:
        sorted_secondary_files.append(save_disk)
    return sorted_secondary_files


def create_temp_m3u(game_name: str, secondary_files: list[Path],
                    temp_dir: PathLike, prog_name: str = None):
    """
    Create an temporary m3u file from the secondary files list

    ### Arguments ###
    * `game_name`       - the name of the game to create the m3u file for
    * `secondary_files` - a list of files to include in the m3u, in order
    * `temp_dir`        - the directory to create the m3u in
    * `prog_name`       - the name of a custom program to load instead of `*`

    ### Returns ###
    The path to the m3u file
    """
    if platform.system() == "Windows" or ARGS.always_windows_safe_names:
        game_name = game_name.translate(WINDOWS_SAFE_REPLACEMENTS)
    m3u_file_path = Path(temp_dir, f"{game_name}.m3u")
    with open(m3u_file_path, "w", encoding="utf-8") as m3u_file:
        for index, file in enumerate(secondary_files):
            file_name = file.name
            if index == 0 and prog_name:
                file_name = f"{file_name}:{prog_name}"
            m3u_file.write(f"{file_name}\n")
    return m3u_file_path.absolute()


def export_gamelist(games_dict, out_dir):
    """
    Export the games dictionary into a gamelist.xml file
    for use in Batocera
    """

    LOG.info("Exporting game list")
    game_list = Element("gameList")
    for game in games_dict.values():
        if game.name == ARGS.game_trace:
            LOG.log(GAME_TRACE,
                    """
                    Final game entry:\n
                    %s
                    """,
                    ElementTree.tostring(game.to_gamelist_xml(), encoding="unicode"))
        game_list.append(game.to_gamelist_xml())

    if ARGS.append_to_gamelist:
        existing_file = ARGS.append_to_gamelist
        # Append the games to the existing gamelist.xml
        if not existing_file.exists():
            raise FileNotFoundError(
                f"Could not find existing gamelist: {existing_file}")
        existing_xml = ElementTree.parse(existing_file)
        existing_list = existing_xml.getroot()
        if not existing_list or not existing_list.tag == "gameList":
            raise IOError(
                f"{existing_file} does not appear to be a valid \
                    gamelist.xml file")
        existing_list.extend(game_list.findall("game"))
        with open(existing_file, "w", encoding="utf-8") as file:
            ElementTree.indent(existing_list)
            file.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
            file.write(ElementTree.tostring(existing_list, encoding="unicode"))
    else:
        # Write a new gamelist.xml
        ElementTree.indent(game_list)
        with open(f"{out_dir}/gamelist.xml", "w", encoding="utf-8") as file:
            file.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
            file.write(ElementTree.tostring(game_list, encoding="unicode"))


def convert_region(region):
    """
    Convert launchbox region names to ES abbreviations
    """
    if not region:
        return None
    elif region == "United States" or region == "USA" or region == "North America":
        return "us"
    elif region == "Europe":
        return "eu"
    elif region == "Japan":
        return "jp"
    elif region == "China":
        return "cn"
    elif region == "Korea":
        return "kr"
    elif region == "Taiwan":
        return "tw"
    elif region == "Hong Kong":
        return "hk"
    elif region == "United Kingdom":
        return "uk"
    elif region == "France":
        return "fr"
    elif region == "Germany":
        return "de"
    elif region == "Italy":
        return "it"
    elif region == "Spain":
        return "es"
    elif region == "Australia":
        return "au"
    elif region == "New Zealand":
        return "nz"
    elif region == "The Netherlands":
        return "nl"
    elif region == "Brazil":
        return "br"
    elif region == "World":
        return "wr"
    else:
        LOG.warning("Unknown region: %s", region)
        return region


def find_manuals(games_dict, source_dir: PathLike):
    """
    Find the manual files in the source directory
    """
    LOG.info("Finding manuals")
    found = 0
    missing = 0
    for game in games_dict.values():
        spinner()
        debug_game_log(game.name, "Looking for manual for: %s", game.name)
        game.manual = find_manual_for_game(game, source_dir)
        if game.manual:
            found += 1
        else:
            missing += 1

    LOG.info("Manual Stats: found: %s, missing: %s", found, missing)


def find_manual_for_game(game: Game, source_dir: PathLike):
    """
    Converts the manual path in the game entry from the LR list
    into a local path relative to source_dir if present. Otherwise
    searches the directory where the game rom is located for a file
    with a name ending in "Manual.pdf" and sets that as the manual path.
    If no manual is found, sets the manual path to None
    """

    if game.manual:
        converted = game.manual.replace("\\", "/")
        converted = Path(source_dir, converted)
        if converted.exists():
            LOG.debug("Manual Path is now: %s", converted)
            debug_game_log(
                game.name, "Converted launchbox manual path to local path:\n%s", converted)
            return converted

    # Search the game directory for a file ending with
    # "Manual.cbz" or "Manual.pdf"
    for file in game.rom_dir.iterdir():
        if file.name.endswith("Manual.cbz") or file.name.endswith("Manual.pdf"):

            debug_game_log(game.name, "Found manual with path:\n%s", file)
            return file

    debug_game_log(game.name, "No manual found")
    return None


def find_videos(games_dict, source_videos_dir: PathLike):
    """
    Find the vidoes in the source directory
    """
    for game in games_dict.values():
        if game.video:
            debug_game_log(game.name, "Finding video for: %s", game.name)
            find_video_for_game(game, source_videos_dir)


def find_video_for_game(game: Game, source_videos_dir: PathLike):
    # pylint: disable=unused-argument
    """
    Gathers the path data for the video the game is supposed to have
    """
    LOG.log(DETAIL, "Videos not available, skipping game: %s", game.name)


def find_images(games_dict, source_images_dir: PathLike, fast_images: bool,
                box_3d_preference: Box3DPreference, allow_fanart: bool):
    """
    Find the images in the source directory
    """
    LOG.info("Finding images")
    found = {
        "screenshots": 0,
        "marquee": 0,
        "box_front": 0,
        "box_back": 0
    }
    missing = {
        "screenshots": 0,
        "marquee": 0,
        "box_front": 0,
        "box_back": 0
    }

    for game in games_dict.values():
        debug_game_log(game.name, "Finding images for: %s", game.name)
        (screenshot, marquee, box_front, box_back) = find_images_for_game(game,
                                                                          source_images_dir,
                                                                          fast_images,
                                                                          box_3d_preference,
                                                                          allow_fanart)

        if screenshot:
            found["screenshots"] += 1
        else:
            missing["screenshots"] += 1

        if marquee:
            found["marquee"] += 1
        else:
            missing["marquee"] += 1

        if box_front:
            found["box_front"] += 1
        else:
            missing["box_front"] += 1

        if box_back:
            found["box_back"] += 1
        else:
            missing["box_back"] += 1

    LOG.info("Screenshot Stats: found: %s, missing: %s",
             found["screenshots"], missing["screenshots"])
    LOG.info("Marquee Stats: found: %s, missing: %s", found["marquee"],
             missing["marquee"])
    LOG.info("Box Front Stats: found: %s, missing: %s", found["box_front"],
             missing["box_front"])
    LOG.info("Box Back Stats: found: %s, missing: %s", found["box_back"],
             missing["box_back"])


def find_images_for_game(game: Game, source_images_dir: PathLike,
                         fast_images: bool, box_3d_preference: Box3DPreference,
                         allow_fanart: bool = False):
    """
    Gathers the path data for the images the game is supposed to have

    ### Returns ###
    A tuple of (screenshot, marquee, box front, box back)
    indicating whether each image type was found
    """
    found_screenshot = False
    found_marquee = False
    found_box_front = False
    found_box_back = False
    if not fast_images or game.image:
        spinner()
        game.image = find_screenshot(game, source_images_dir)
        if game.image:
            found_screenshot = True

    if not fast_images or game.marquee:
        spinner()
        game.marquee = find_marquee(game, source_images_dir)
        if game.marquee:
            found_marquee = True

    if not fast_images or game.thumbnail:
        spinner()
        game.thumbnail = find_box_front(game, source_images_dir,
                                        box_3d_preference, allow_fanart)
        if game.thumbnail:
            found_box_front = True
        # The metadata doesn't have an indicator for box backs, so
        # assume if there is a box front, there is likely a box back
        game.boxback = find_box_back(game, source_images_dir, allow_fanart)
        if game.boxback:
            found_box_back = True

    return (found_screenshot, found_marquee, found_box_front, found_box_back)


def find_screenshot(game: Game, source_images_dir: PathLike):
    """
    Gathers the path data for the screenshot the game is supposed to have
    Preference order is:
      * gameplay
      * game title
      * game select
      * game over
      * high scores

    ### Returns ###
    The path to the screenshot, or none if the screenshot could not be found
    """
    debug_game_log(game.name, "Finding screenshot for: %s", game.name)
    dirs = (
        C64D_SCREENSHOT_GAMEPLAY_DIR,
        C64D_SCREENSHOT_TITLE_DIR,
        C64D_SCREENSHOT_SELECT_DIR,
        C64D_SCREENSHOT_GAME_OVER_DIR,
        C64D_SCREENSHOT_HIGH_SCORES_DIR
    )

    image = None
    for image_dir in dirs:
        image = search_for_image_in_dir(
            game,
            Path(source_images_dir, image_dir)
        )
        if image:
            debug_game_log("Found screenshot: %s", image)
            break

    if not image:
        debug_game_log(game.name, "No screenshot found")

    return image


def find_marquee(game: Game, source_images_dir: PathLike):
    """
    Gathers the path data for the marquee the game is supposed to have
    Preference order is:
      * Clear Logo
      * Banner
      * Arcade Marquee
    """
    debug_game_log(game.name, "Finding marquee for: %s", game.name)
    dirs = (
        C64D_CLEAR_LOGO_DIR, C64D_BANNER_DIR, C64D_ARCADE_MARQUEE_DIR
    )

    image = None
    for image_dir in dirs:
        image = search_for_image_in_dir(
            game,
            Path(source_images_dir, image_dir)
        )
        if image:
            debug_game_log(game.name, "Found marquee: %s", image)
            break

    if not image:
        debug_game_log(game.name, "No marquee found")

    return image


def find_box_front(game: Game, source_images_dir: PathLike,
                   box_3d_preference: Box3DPreference,
                   allow_fanart: bool):
    """
    Gathers the path data for the box front the game is supposed to have
    Preference order is:
      * 3D Boxes (if preferred)
      * Box front
      * Reconstructed box front
      * 3D Boxes (if allowed)
      * Fanart box front (if allowed)
      * Fanart disk image (if allowed)
      * Cartridge front
    """
    debug_game_log(game.name, "Finding box front for: %s", game.name)
    dirs = [
        C64D_BOX_FRONT_DIR,
        C64D_BOX_FRONT_RECONSTRUCTED_DIR,
        C64D_CART_FRONT_DIR,
    ]

    if allow_fanart:
        dirs.insert(2, C64D_BOX_FRONT_FANART_DIR)
        dirs.insert(3, C64D_DISK_FANART_DIR)

    if box_3d_preference == Box3DPreference.PREFER:
        dirs.insert(0, C64D_3D_BOX_DIR)
    elif box_3d_preference == Box3DPreference.ALLOW:
        dirs.insert(2, C64D_BOX_FRONT_FANART_DIR)

    image = None
    for image_dir in dirs:
        image = search_for_image_in_dir(
            game,
            Path(source_images_dir, image_dir)
        )
        if image:
            debug_game_log(game.name, "Found box front: %s", image)
            break

    if not image:
        debug_game_log(game.name, "No box front found")

    return image


def find_box_back(game: Game, source_images_dir: PathLike,
                  allow_fanart: bool = False):
    """
    Gathers the path data for the box back the game is supposed to have
    Preference order is:
      * Box back
      * Reconstructed box back
      * Fanart box back (if allowed)
      * Cartridge back
    """
    debug_game_log(game.name, "Finding box back for: %s", game.name)
    dirs = [
        C64D_BOX_BACK_DIR,
        C64D_BOX_BACK_RECONSTRUCTED_DIR,
        C64D_CART_BACK_DIR,
    ]

    if allow_fanart:
        dirs.insert(2, C64D_BOX_BACK_FANART_DIR)

    image = None
    for image_dir in dirs:
        image = search_for_image_in_dir(
            game,
            Path(source_images_dir, image_dir)
        )
        if image:
            debug_game_log(game.name, "Found box back: %s", image)
            break

    if not image:
        debug_game_log(game.name, "No box back found")

    return image


def search_for_image_in_dir(game: Game, image_dir: PathLike):
    """
    Searches a directory for an image for a game.
    Sometimes the names have their special characters stripped and
    sometimes they dont, so we need to search for both
    """

    ## SPECIAL HANDLING ##
    # Special cases first to save searching:
    if not ARGS.suppress_image_detail_logging:
        LOG.log(DETAIL, "Processing special case image search for game: %s", game.name)

    special_case_name = None
    if game.name.startswith("Psi 5"):
        special_case_name = "Psi-5 Trading Company"
    elif game.name == "The Odyssey":
        special_case_name = "Odyssey, The"
    elif game.name == "Super Pipeline II":
        special_case_name = "Super Pipeline 2"
    elif game.name == "The Life of a Lone Electron":
        special_case_name = "Life of a Lone Electron"
    elif game.name == "Frogger II: ThreeeDeep!":
        image = search_for_image_with_name(
            "Frogger II_ Threeedeep!", image_dir)
        if image:
            return image
        special_case_name = "Frogger II_ ThreeeDeep!"
    elif game.name == "Hell Hole":
        special_case_name = "Hellhole"
    elif game.name == "Olé!":
        special_case_name = "Ole!"
    elif game.name == "Pirates!":
        special_case_name = "Pirates"
    elif game.name == "Euro City Cops!":
        special_case_name = "Euro City Cops"
    elif game.name == "River-Raid II: The River Patrol":
        special_case_name = "River Raid 2"

    if special_case_name:
        debug_game_log("Special case image processing: %s", special_case_name)
        image = search_for_image_with_name(special_case_name, image_dir)
        if image:
            return image

    ## END SPECIAL HANDLING ##

    game_name = game.name.translate(WINDOWS_SAFE_REPLACEMENTS)
    image = search_for_image_with_name(game_name, image_dir)
    if image:
        return image

    if ":" in game.name:
        # In some cases, the `:` is replaced with ` -` instead of `_` so
        # try for that instead.
        game_name = game.name.translate(WINDOWS_SAFE_ALTERNATES)
        image = search_for_image_with_name(game_name, image_dir)
        if image:
            return image

    # Try with the special characters preserved
    # Some special characters that are normally replaced are actually safe
    # for windows paths (like apostrophes), so try without if we didn't find
    # a replaced version
    image = search_for_image_with_name(game.name, image_dir)
    if image:
        return image


def search_for_image_with_name(game_name: str, image_dir: PathLike):
    """
    Searches a directory for an image for a game.
    First we look for any image named "${game.name}.png"
    Then we try for "${game.name}-01.png"
    Then finaly any image with starting with the game name
    and ending in ".png" or ".jpg"
    """

    image = Path(image_dir, f"{game_name}.png")
    if image.exists():
        return image

    image = Path(image_dir, f"{game_name}-01.png")
    if image.exists():
        return image

    if image_dir in IMAGE_DIR_CACHE:
        files = IMAGE_DIR_CACHE.get(image_dir)
    else:
        cache = list()
        for root, _, files in os.walk(image_dir):
            for file in files:
                cache.append(Path(root, file))
        cache.sort(key=lambda x: x.name)
        IMAGE_DIR_CACHE[image_dir] = cache
        files = cache

    search_start = bisect_left(files, game_name, key=lambda x: x.name)
    # If we don't find it within 10 files we almost certainly won't find a file at all
    search_end = min(len(files), search_start + 11)
    for i in range(search_start, search_end):
        file = files[i]
        if re.match(re.escape(game_name) + r"(-[0-9]+)?\.(?:png|jpg)", file.name):
            return file

    return None


def move_files(games_dict, out_dir: PathLike):
    """
    Move the ROMs to the output directory
    """

    log_action = "Copying" if ARGS.copy_files else "Moving"
    LOG.info("%s files to destination", log_action)

    use_windows_safe_name = ARGS.always_windows_safe_names \
        or platform.system() == "Windows"
    if ARGS.copy_files:
        def move_method(source: Path, dest: Path):
            if ARGS.replace_existing and dest.exists():
                os.remove(dest)
            shutil.copy2(source, dest)
    else:
        def move_method(source: Path, dest: Path):
            if ARGS.replace_existing and dest.exists():
                os.remove(dest)
            dest.symlink_to(source)

    if ARGS.isolate_roms:
        rom_dir = Path(out_dir, ARGS.isolate_roms)
        if not rom_dir.exists():
            rom_dir.mkdir()

    for game in games_dict.values():
        spinner()
        move_game(game, out_dir, move_method,
                  use_windows_safe_name)


def move_game(game: Game, out_dir: PathLike, move_method: Callable,
              use_windows_safe_name: bool):
    """
    Move a single game to the output directory
    """

    # Determine if we need to use windows safe game names
    if use_windows_safe_name:
        safe_game_name = game.name.translate(WINDOWS_SAFE_REPLACEMENTS)
        if safe_game_name.endswith("."):
            safe_game_name = safe_game_name[:-1]
    else:
        # If not windows, the only thing we need to replace in any file names
        # is the `/` which will be interpreted by python's path as a separator
        safe_game_name = game.name.replace("/", "_")

    # Create the main ROM directory
    if ARGS.isolate_roms:
        game_dir = Path(out_dir, ARGS.isolate_roms, safe_game_name)
    else:
        game_dir = Path(out_dir, safe_game_name)
    if not game_dir.exists():
        game_dir.mkdir()

    # If this isn't a .m3u multi disk game, we can rename the files to something
    # more informative than just `Disk1.d64` or similar.
    can_rename_generic_files = not game.files_to_move[0].suffix == ".m3u"

    for index, file in enumerate(game.files_to_move):

        # Rename generic files if needed
        if not ARGS.preserve_generic_rom_names and can_rename_generic_files:
            generic_matcher = re.match(r'(Disk[0-9]+)\..*', file.name)
            if generic_matcher:
                file_name = f"{safe_game_name}_{generic_matcher.group(1)}{file.suffix}"
                debug_game_log(game.name, "Renamed generic file: %s -> %s",
                               file.name, file_name)
            else:
                debug_game_log(game.name, "Skipping generic file renaming because %s \
                               did not match pattern", file.name)
                file_name = file.name
        else:
            if ARGS.preserve_generic_rom_names:
                debug_game_log(game.name,
                               "Skipping generic file renaming at user request")
            else:
                debug_game_log(
                    game.name,
                    "Skipping generic file renaming because %s is a multi \
                        disk game with an m3u file", game.name)
            file_name = file.name

        dest_target = Path(game_dir, file_name)
        move_method(file, dest_target)
        if index == 0:
            game.path = f"./{game_dir.relative_to(out_dir).as_posix()}/{file_name}"

        debug_game_log(game.name, "Moved %s to %s", file, dest_target)

    if game.manual:
        manual_name = game.manual.name.replace("/", "_")
        manual_dest = Path(out_dir, "manuals", manual_name)
        try:
            move_method(game.manual, manual_dest)
        except FileExistsError:
            # Multiple games might point to the same manual
            LOG.log(DETAIL, "Manual named %s already exists, skipping", manual_name)

        debug_game_log(game.name, "Moved %s to %s", game.manual, manual_dest)
        game.manual = "./manuals/" + manual_name

    if game.image:
        dest_name = f"{safe_game_name}-image{game.image.suffix}"
        image_dest = Path(
            out_dir, "images", dest_name)
        move_method(game.image, image_dest)

        debug_game_log(safe_game_name, "Moved %s to %s",
                       game.image, image_dest)
        game.image = "./images/" + dest_name

    if game.marquee:
        dest_name = f"{safe_game_name}-marquee{game.marquee.suffix}"
        marquee_dest = Path(
            out_dir, "images", dest_name)
        move_method(game.marquee, marquee_dest)

        debug_game_log(game.name, "Moved %s to %s", game.marquee, marquee_dest)
        game.marquee = "./images/" + dest_name

    if game.thumbnail:
        dest_name = f"{safe_game_name}-thumb{game.thumbnail.suffix}"
        thumbnail_dest = Path(
            out_dir, "images", dest_name)
        move_method(game.thumbnail, thumbnail_dest)

        debug_game_log(game.name, "Moved %s to %s",
                       game.thumbnail, thumbnail_dest)
        game.thumbnail = "./images/" + dest_name

    if game.boxback:
        dest_name = f"{safe_game_name}-boxback{game.boxback.suffix}"
        boxback_dest = Path(
            out_dir, "images", dest_name)
        move_method(game.boxback, boxback_dest)

        debug_game_log(game.name, "Moved %s to %s", game.boxback, boxback_dest)
        game.boxback = "./images/" + dest_name


def spinner():
    """
    Simple spinner for the terminal output
    """
    spinner_char = next(SPINNER)
    print(f"\b{spinner_char}", end="", flush=True)


class RatingSource(Enum):
    """
    Whether to use the official or community rating for the games
    """
    OFFICIAL = 0,
    COMMUNITY = 1,


class Box3DPreference(Enum):
    """
    Enum for the user preference for 3d box handling
    """
    DISALLOW = 0,
    ALLOW = 1,
    PREFER = 2,

    @ classmethod
    def parse_arg(cls, value: str) -> Box3DPreference:
        """
        Convert from the command line argument to the enum value
        """
        if value == "disallow":
            return cls.DISALLOW
        elif value == "allow":
            return cls.ALLOW
        elif value == "prefer":
            return cls.PREFER


class Game:
    """
    Represents a single Game for addition to the emulation station game list
    """

    def __init__(self,
                 game_id: str,
                 name: str,
                 path: PathLike = None,
                 md5: str = None,
                 description: str = None,
                 image: str = None,
                 video: str = None,
                 marquee: str = None,
                 thumbnail: str = None,
                 manual: str = None,
                 map_image: str = None,
                 boxback: str = None,
                 rating: str = None,
                 release_date: datetime = None,
                 developer: str = None,
                 publisher: str = None,
                 genre: str = None,
                 family: str = None,
                 players: str = None,
                 lang: str = None,
                 region: str = None,
                 root_folder: PathLike = None,
                 ):
        """
        Create a new Game record

        ### Arguments ###

        #### Emulation Station Properties ####
        * `game_id` - A unique id of the game
        * `name` - The display name of the game
        * `path` - The path to the game
        * `md5` - The MD5 hash of the game
        * `description` - The description of the game
        * `image` - The path to the main screenshot to use in ES
                    (should be in images subfolder)
        * `video` - The path to the video to use in ES
        * `marquee` - The path to the marquee image to use in ES
                    (should be in images subfolder)
        * `thumbnail` - The path to the box front image to use in ES
                        (should be in images subfolder)
        * `manual` - The path to the manual to use in ES
                    (should be in manuals subfolder)
        * `map_image` - The path to the map to use in ES
                  (should be in images subfolder)
        * `boxback` - The path to the boxback to use in ES
                      (should be in images subfolder)
        * `rating` - The numerial rating(out of 5.0) of the game
        * `release_date` - The release date of the game
        * `developer` - The developer of the game
        * `publisher` - The publisher of the game
        * `genre` - The genre of the game
        * `family` - The family of the game(for games that are part of a
                     series or collection)
        * `players` - The number of players  that can play the game
        * `lang` - The language of the game
        * `region` - The release region of the game

        #### Other Useful Properties ####
        * `root_folder` - The root folder of the game in the C64 Dreams
                          Collection
        """
        self.game_id = game_id
        self.name = name
        self.path = path
        self.md5 = md5
        self.desccription = description
        self.image = image
        self.video = video
        self.marquee = marquee
        self.thumbnail = thumbnail
        self.manual = manual
        self.map_image = map_image
        self.boxback = boxback
        self.rating = rating
        self.release_date = release_date
        self.developer = developer
        self.publisher = publisher
        self.genre = genre
        self.family = family
        self.players = players
        self.lang = lang
        self.region = region
        self.root_folder = root_folder
        self.files_to_move = list()

    @ staticmethod
    def from_launchbox_game(game: Element, rating_source: RatingSource = RatingSource.COMMUNITY):
        """
        Pull all the data out of the launchbox game list and convert to
        the data for an emulation station game instead. Note this does
        not alter the paths, that is up to other functions. This only
        captures the data we're interested in out of the launchbox XML

        When we capture the data from the launchbox, the launchbox list
        doesn't contain paths to the images and marquees. Instead we will take
        in the boolean value that indicates whether the item is missing and
        later we will use that to find the images when we need to move things
        and convert the paths.
        """
        has_video = game.find("MissingVideo").text != 'true'
        has_screenshot = game.find("MissingScreenshotImage").text != 'true'
        has_marquee = game.find("MissingClearLogoImage").text != 'true'
        has_box_front = game.find("MissingBoxFrontImage").text != 'true'
        has_3d_box = game.find("MissingBox3dImage").text != 'true'

        if has_3d_box or has_box_front:
            box_path = "true"
        else:
            box_path = "false"

        if rating_source == RatingSource.OFFICIAL:
            rating = float(game.find("StarRating").text) / 5
        else:
            rating = float(game.find("CommunityStarRating").text) / 5

        # LB uses different region names than ES
        region = convert_region(game.find("Region").text)

        # Not all entries have a MaxPlayers element
        max_players = game.find("MaxPlayers")
        if max_players:
            max_players = max_players.text

        # Not all entries have a release date
        release_date = game.find("ReleaseDate")
        if release_date:
            release_date = datetime.fromisoformat(release_date.text)

        return Game(
            game_id=game.find("ID").text,
            name=game.find("Title").text,
            path=game.find("ApplicationPath").text,

            md5=None,

            description=game.find("Notes").text,

            # These will get converted to real paths
            # during the parsing process
            image=has_screenshot,
            video=has_video,
            marquee=has_marquee,
            thumbnail=box_path,

            manual=game.find("ManualPath").text,

            map_image=None,
            boxback=None,

            rating=rating,
            release_date=release_date,
            developer=game.find("Developer").text,
            publisher=game.find("Publisher").text,
            genre=game.find("Genre").text,
            family=game.find("Series").text,
            players=max_players,
            region=region,

            lang=None,

            root_folder=game.find("RootFolder").text,
        )

    def add_file(self, file: Path):
        """
        Add a file that this game is associate with to the
        list of files to move
        """
        self.files_to_move.append(file)

    def to_gamelist_xml(self):
        """
        Convert the game to a gamelist.xml element
        """
        game = Element("game")
        game.set("id", self.game_id)
        SubElement(game, "name").text = self.name
        SubElement(game, "path").text = self.path

        if self.md5:
            SubElement(game, "md5").text = self.md5

        if self.desccription:
            SubElement(game, "desc").text = self.desccription

        if self.image:
            SubElement(game, "image").text = str(self.image)

        if self.video:
            SubElement(game, "video").text = str(self.video)

        if self.marquee:
            SubElement(game, "marquee").text = str(self.marquee)

        if self.thumbnail:
            SubElement(game, "thumbnail").text = str(self.thumbnail)

        if self.manual:
            SubElement(game, "manual").text = str(self.manual)

        if self.map_image:
            SubElement(game, "map").text = self.map_image

        if self.boxback:
            SubElement(game, "boxback").text = str(self.boxback)

        if self.rating and float(self.rating) > 0:
            SubElement(game, "rating").text = str(self.rating)

        if self.release_date:
            SubElement(game, "release_date").text = self.release_date\
                .strftime("%Y%m%dT%H%M%S")

        if self.developer:
            SubElement(game, "developer").text = self.developer

        if self.publisher:
            SubElement(game, "publisher").text = self.publisher

        if self.genre:
            SubElement(game, "genre").text = self.genre

        if self.family:
            SubElement(game, "family").text = self.family

        if self.players and int(self.players) > 1:
            SubElement(game, "players").text = self.players

        if self.region:
            SubElement(game, "region").text = self.region

        if self.lang:
            SubElement(game, "lang").text = self.lang

        return game


class CommercialGameError(Exception):
    """
    Thrown when we encounter a commercial game, we're just skipping
    them for now
    """


class SpecialHandlingError(Exception):
    """
    Thrown when we encounter a game requiring special handling,
    we're just skipping them for now
    """

    def __init__(self, reason):
        self.reason = reason


class NeedsManualM3UError(SpecialHandlingError):
    """
    Thrown when we encounter a game requiring manual m3u. Try to catch this
    and process a manual M3U
    """


### Code for rsyncing the files to batocera ###

def rsync():
    """
    Rsync the files to batocera
    """

    cmd = [
        "rsync",
        "--verbose",
        "--archive",
        "--copy-links",
        "--human-readable",
        "--progress",
        "--omit-dir-times",
        "--exclude=temp/",
    ]

    if ARGS.dry_run:
        cmd.append("--dry-run")

    if ARGS.delete:
        cmd.append("--delete")

    # rsync has specific ways of understanding a source argument so don't
    # resolve or mess with the one provided by the user
    cmd.append(ARGS.source)
    cmd.append(ARGS.destination)

    if ARGS.dry_run:
        LOG.info("Running rsync in dry run mode")
    else:
        LOG.info("Running rsync")

    subprocess.run(cmd, check=True)

    if ARGS.keep_temp:
        LOG.info("Keeping temp files")
    else:
        clean_up_temp_dir(Path(ARGS.source, "temp"), ARGS.dry_run)


def clean_up_temp_dir(temp_dir: Path, dry_run: bool = False):
    """
    Clean up the tempory directory used for manual m3u file creation. This will
    either automatically get removed during parsing if the `--copy-files` flag
    was used, or it will get removed duing the rsync of `--keep-temp` was not
    used.

    ### Arguments ###
    * `temp_dir` - The directory to clean up and remove
    * `dry_run` - Whether or not we're running in dry run mode. If True, don't
                  actually delete anything just report what we would do
    """
    LOG.info("Cleaning up temp files")
    for root, dirs, files in os.walk(temp_dir):
        for file in files:
            if dry_run:
                print(f"Delete {file}")
            else:
                os.remove(os.path.join(root, file))
        for directory in dirs:
            if dry_run:
                print(f"Delete {directory}")
            else:
                os.rmdir(os.path.join(root, directory))
        if dry_run:
            print(f"Delete {root}")
        else:
            os.rmdir(root)


def debug_game_log(game_name: str, log_message: str, *args, **kwargs):
    """
    Log a game specific debug message, if the game is one we're tracing
    output the log as a game trace message instead of a debug message
    """
    if game_name == ARGS.game_trace:
        log_level = GAME_TRACE
    else:
        log_level = logging.DEBUG
    LOG.log(log_level, log_message, *args, **kwargs)


def parse_args(user_config: dict):
    """
    Parse the command line arguments

    Argparse's help output is pretty basic in terms of
    clean formatter, so the RawTextHelpFormatter is used
    and help is formatted for 80 column display
    """

    epilog = textwrap.dedent(f"""
        ### Configuration Files ###
        {CONFIG_FILES}

        ### Known Issues ###
        {KNOWN_ISSUES}

        ### Special Thanks ###
        {SPECIAL_THANKS}
""")

    main_parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=epilog
    )

    main_parser.add_argument(
        "--log-level",
        choices=["debug", "detail", "info", "warning", "error"],
        default="info",
        help=textwrap.dedent("""\
            The logging level to output
            """)
    )

    subparsers = main_parser.add_subparsers(
        dest="mode",
        help=textwrap.dedent("""
            Which mode to run in. Further help is available for
            each mode
            """)
    )

    parser = subparsers.add_parser(
        "parse",
        help=textwrap.dedent("""
            Parse the C64 Dreams Launchbox Collection and setup the
            output directory
            """),
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "source",
        help=textwrap.dedent("""
            The source dir containing the C64 Dreams Launchbox
            Collection
            """)
    )

    parsing_options = parser.add_argument_group(
        "Parsing Options",
        "Options for controlling how the game data is parsed and converted"
    )
    parsing_options.add_argument(
        "--allow-fanart-boxes",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            When searching for boxes, allow the fan art boxes if no
            official art is found. Default is to not allow.
            """)
    )

    parsing_options.add_argument(
        "--box-3d",
        choices=["disallow", "allow", "prefer"],
        default="allow",
        help=textwrap.dedent("""\
            When searching for boxes, how to handle the 3D box
            images. Default: `allow`

            * disallow - Do not search use the 3D box image
            * allow    - Use the 3D box image if the 2D image is
                         not available
            * prefer   - Prefer the 3D box image over the 2D image
            """)
    )

    parsing_options.add_argument(
        "--preserve-generic-rom-names",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            Because the Batocera scraper uses file names to search
            the scraper databases, we normally convert convert all
            non-m3u based ROMs with names like `Disk1.d64` to
            `${GAME_NAME}.d64`. Pass this flag if you want to keep
            the original name from the C64 Dreams Collection.
            """)
    )

    parsing_options.add_argument(
        "--fast-images",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            Instead of searching for all images for all games, make
            assumptions about which ones are missing using the
            Launch Box metadata and skip them. Default is to search.
            """)
    )

    output_options = parser.add_argument_group(
        "Output Options",
        "Options for managing the output directory"
    )

    output_options.add_argument(
        "--replace-existing",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            Normally python will error if we try to copy a file or
            create a symlink at a location where a file already
            exists. If this option is passed, we will replace the
            existing file instead. Useful for if you are outputing
            to an existing C64 collection that you don't want to
            completely overwrite with the `--clean` option
            """)
    )

    output_options.add_argument(
        "--always-windows-safe-names",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            Always use Windows safe names for output files.
            Otherwise if running in a *nix OS, we'll use some
            allowed special characters if they are part of the
            game name. Default is to use all charactrs allowed
            by *nix OSes
            """)
    )

    output_options.add_argument(
        "--append-to-gamelist",
        type=Path,
        metavar="GAMELIST_PATH",
        help=textwrap.dedent("""\
            A path to a gamelist.xml file to append to.
            When outputing the gamelist.xml data, append it to this
            existing gamelist.xml file instead of creating a new
            one. Note that this will overwrite the file at this path
            """)
    )

    output_options.add_argument(
        "-o", "--out-dir",
        default=f"{DEFAULT_OUT_DIR}",
        help=textwrap.dedent(f"""\
            The output directory. Defaults to {DEFAULT_OUT_DIR}
            in the current directory
            """)
    )

    output_options.add_argument(
        "--clean",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""
            Clean the output directory before copying. Default is to
            not clean.
            """)
    )

    output_options.add_argument(
        "--copy-files",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            Copy the ROM and other files to the output directory
            rather than using symlinks. Useful if your output
            directory is the Batocera SHARE on the network, though
            that could be very slow given the size of the collection.
            Default is to not copy and use symlinks.
            """)
    )

    output_options.add_argument(
        "--isolate-roms",
        metavar="ROM_DIR_NAME",
        help=textwrap.dedent("""\
            Create a separate directory in the output directory
            for storing the games. This is useful if you are
            planning on combining the C64 Dreams collection with
            other C64 collections and want to keep the C64 Dreams
            roms in their own directory.
            """)
    )

    dev_options = parser.add_argument_group(
        "Dev Options",
        textwrap.dedent("""
        Options useful when working on the code, but not likely useful for
        day-to-day script usage
        """)
    )

    dev_options.add_argument(
        "--game-trace",
        help=textwrap.dedent("""\
            Print information about a specific game as it's
            being parsed
            """)
    )

    dev_options.add_argument(
        "--suppress-image-detail-logging",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            Suppress the logging at the detail level of image
            special cases
            """)
    )

    rsync_parser = subparsers.add_parser(
        "rsync",
        formatter_class=argparse.RawTextHelpFormatter,
        help=textwrap.dedent("""
            Rsync the parsed library to a destination and cleanup
            temp files. Rsync mode does require that you have the
            `rsync` installed. For windows users, either WSL or
            Cygwin is recommended so that you can run this part
            of the script
            """),
    )

    rsync_parser.add_argument(
        "source",
        help=textwrap.dedent("""
            The source dir containing the output of the `parse` mode
            """)
    )

    rsync_parser.add_argument(
        "destination",
        help=textwrap.dedent("""
            The destination for the sync command. This should be in
            the normal format that rsync prefers
            (e.g. [user@]host:[path])
            """)
    )

    rsync_parser.add_argument(
        "--dry-run",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
            Don't actually rsync the files, just print the
            dry-run output from rsync. Default is to rsync
            for real.
            """)
    )

    rsync_parser.add_argument(
        "--delete",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
        Pass the `delete` flag to rsync to delete files on the
        remote side that are not present in the source. Default
        is to not pass the `delete` flag.
        """)
    )

    rsync_parser.add_argument(
        "--keep-temp",
        action=argparse.BooleanOptionalAction,
        help=textwrap.dedent("""\
        After rsyncing, keep the temporary files folder in the
        source dir. Default is to delete the temp files and
        directory.
        """)
    )

    # Override the normal defaults with user config file defaults
    # doing things this way allows the config file to override our defaults
    # from above, while still further overriding them with a command line arg
    parser.set_defaults(**user_config["parse"])
    rsync_parser.set_defaults(**user_config["rsync"])
    args = main_parser.parse_args()
    return args


def load_config() -> dict:
    """
    Loads user configurations from the config file if it exists, this allows
    the user to save their default options in a config file rather than
    specify them in the command line all the time.

    Returns:
        dict: The loaded configuration
    """
    config_file = Path(Path.home(), ".config/c64dreams2batocera.toml")
    if config_file.exists():
        with open(config_file, "rb") as f:
            cfg_parser = tomllib.load(f)
            return cfg_parser
    else:
        return None


if __name__ == "__main__":

    logging.basicConfig(format="%(levelname)s: %(message)s")
    logging.addLevelName(DETAIL, "DETAIL")

    config = load_config()
    ARGS = parse_args(config)

    if ARGS.mode == "parse" and ARGS.game_trace:
        logging.addLevelName(GAME_TRACE, ARGS.game_trace.upper())
    else:
        logging.addLevelName(GAME_TRACE, "GAME_TRACE")

    LOG.setLevel(ARGS.log_level.upper())

    if ARGS.mode == "parse":
        parse()
    elif ARGS.mode == "rsync":
        rsync()
