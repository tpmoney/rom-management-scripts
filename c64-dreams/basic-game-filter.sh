#! /bin/bash

#find ./ -type f \
#  -not -name "*.d64" \
#  -not -name "*.crt" \
#  -not -name "*.m3u" \
#  -not -name "*.t64" \
#  -not -name "*.d81" \
#  -not -name "*.D64" \
#  -not -name "*.g64" \
#  -not -name "*.tcrt" \
#  \

find ./ -type f \
  -not -name "*.bat" \
  -not -name "*.cmd" \
  -not -name "*.vbs" \
  -not -name "*.cbz" \
  -not -name "*.url" \
  -not -name "*.ahk" \
  -not -name "*.state.auto" \
  -not -name "*.pdf" \
  -not -name "*.txt" \
  -not -name "*.png" \
  -not -name "*.jpg" \
  -not -name "*.gif" \
  -not -name ".DS_Store" \
  -not -name "*.html" \
  -not -name "*.mp3" \
  -not -name "vicerc" \
  -not -name "*.exe" \
  -not -name "*savegametool*" \
  -not -name "*.nfo" \
  -not -name "*.rtf" \
  -not -name "*.vpl" \
  -not -name "*.odt"
