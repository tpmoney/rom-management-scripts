# C64 Dreams Collection Processing #

The scripts in this directory are designed to help process the C64 Dreams
Collection[c64-dreams] into a format suitable for use with
Batocera Linux[batocera]. To that end there are two possible ways to handle the
migration to Batocera.

First are the `basic-game-*` scripts which are quick
and dirty brute force copies of the ROMs and only the ROMs from the collection.
While this works, so many of the files are named `Disk1.d64` or similar that it
makes the scraper in Batocera ineffective for those games as it used the ROM
file name to prime its searches.

The second and better method is the `c64dreams-2-batocera.py` script. This
script is designed to try to bring the incredible detail of the collection
into Batocera. To that end it parses the collection data and tries to build a
valid Batocera collection, complete with gamelist and images. This is the
recommended way to go and more details about running the script can be found in
the `--help` option.

The script was last tested on C64 Dreams v0.60. It currently can produce a ROMS
directory with m3u playlists for multi disk games, the gamelist.xml and
appropriate image files. Additionally it has support for symlinking to the
original files rather than making a hard copy to save disk space. The `rsync`
mode for the script passes the correct flags to turn symlinks into their target
files durign the copy.

As of the current collection, the script only fails to convert two games which
are missing files specified in their m3u playlist.

Future goals include:

* Building custom Batocera collections based on the curated lists in the
  C64 Dreams Collection.

* Handling of games with "Alt" versions

* Creating a collection for magazines

* Taking advantage of the collection's efforts to simplify and organize the game
  control schemes

## Configuration Files ##

This script will search for a TOML file called `c64dreams2batocera.toml` in the
user's ${HOME}/.config directory. This file can contain two sections `parse`
and `rsync` and will be used to configure the default behavior of the script.
Any optional arguments can be specified in this file. Arguments with a `-` in
the name should instead be written in snake_case. For optional arguments that
enable an action (e.g. the `--clean` option), specify them with a `true` or
`false` value. Any argument in the config file can be overridden by passing the
same argument in the command line. For arguments that enable an action, specify
the equivalent `no-action` argument on the command line to disable the action.

### Example Config ###

The following is an example config file that sets the following arguments for
parse and rsync modes:
`parse --clean --isolate-roms=c64_dreams`
`rsync --delete --keep-temp`

```toml
[parse]
clean = true
isolate-roms = "c64_dreams"

[rsync]
delete = true
keep-temp = true
```

If using the above config, you wanted to not clean the output directory when
parsing, you would pass `--no-clean` on the command line to override the `clean`
directive in the config.

## Known Issues ##

* RetroBat and `.d81` images - While this script should be suitable for any
Emulation Station based system, not all systems ship with an emulator that
handles the `d81` format. A user on the C64 Dreams discord reports that RetroBat
is one such system and that `d81` games do not appear in the game list when
using this script.

* M3U Files that Specify a Specific Program - libretro:vice x64 allows an m3u
file to specify a specific program to run (instead of `*`) on launch by
appending a `:program_name` to the end of the disk entry in the file. There is
a bug in Batocera / Emulation Station which causes these games to display as a
directory of games instead of a single game. The game will still work and launch
properly, you just need to select the item without any disk names in it from the
Emulation Station UI.

## C64 Collection Specific Notes ##

### Games Where the cmd File Specifies a Program ###

For these games, we create a custom m3u file that specifies the correct program
to run. As noted in Known Issues above, this means that the game will display
incorrectly in the Batocera UI.

## Special Thanks ##

* [Zombeaver][c64-dreams] - As the author and compiler of the C64
Dreams collection, none of this would be possible without the many hours of work
they have put into curating the collection.

* [The Batocera Linux Team][batocera] - Their efforts have put
together an incredible retro gaming multi system with excellent defaults and
support for a number of different systems and hardware.

* Boris Schneider-Johne - A user from the C64 Dreams Discord server who has
provided invaluable feedback and testing of this script in Windows environments.

And lastly all the C64 developers and authors through the years who have made
so many incredible games and memories for us all, and have inspired so many to
work so dilligently to keep the memories alive for everyone.

[c64-dreams]: https://www.zombs-lair.com/blog/categories/c64-dreams
[batocera]: https://www.batocera.org
