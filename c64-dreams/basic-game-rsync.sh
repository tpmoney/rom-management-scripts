#! /bin/bash

rsync -arv \
      --progress \
      --include "*.m3u" \
      --include "*.d64" \
      --include "*.crt" \
      --include "*.d81" \
      --include "*.t64" \
      --include "*.d81" \
      --include "*.D64" \
      --include "*.g64" \
      --include "*.tcrt" \
      --exclude "*/Zzap Review/" \
      --exclude "!*" \
      --include "*/" \
      --exclude "*"  \
      ./ root@batocera:/userdata/roms/c64/
