#! /bin/bash

if [ -z $1 ]; then
  echo "Usage $0 DIRECTORY"
fi

find  -L ./ -type l -exec rm -f {} \;
