# Batocera Scripts

These two scripts are designed to be run on the Batocera box itself. Copy them
to someplace like `/userdata/sys/bin` and run them from there. To run them you
may need to give them the executable permission with `chmod +x ${file}`.
