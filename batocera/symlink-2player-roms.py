#! /usr/bin/env python3
"""
A script designed to be run on your Batocera system to create the 2-player
gameboy and gameboy color collections based on player data in `gamelist.xml`.
This will create symlinks between a the main system and its associate
2-player system collection so that the ROMs don't need to be duplicated.

Copyright © 2023 Tevis Money

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
# pylint: disable=invalid-name
#   Kebab case script names are more common in batocera

import argparse
from collections.abc import Iterable
import logging
from logging import debug, info, warning
import os
from os import path
from os import PathLike
import xml.etree.ElementTree as xml

DESCRIPTION = """\
Create symlinks for 2 player rom folder from their parent dir.

Supports the Gameboy and Gameboy Color systems
"""

DEFAULT_ROMS_DIR = "/userdata/roms"

DEFAULT_GAMELIST_NAME = "gamelist.xml"


def main():
    """
    Main function to run everything
    """
    source_dir = path.join(args.roms_dir, args.sys)
    dest_dir = path.join(args.roms_dir, f"{args.sys}2players")
    gamelist = path.join(source_dir, args.gamelist)

    info(f"Source: {source_dir}")
    info(f"Dest: {dest_dir}")
    info(f"Game List: {gamelist}")

    multi_player_games = parse_gamelist(source_dir, gamelist)
    if args.remove:
        remove_existing(dest_dir)

    link_games(multi_player_games, dest_dir)


def parse_gamelist(source_dir: PathLike, gamelist: PathLike) -> set[PathLike]:
    """
    Parse the `gamelist.xml` file for the chosen system to find all of the
    two player games.

    ### Args ###
    - `source_dir`: The directory containing the roms to link
    - `gamelist`: The path to the `gamelist.xml` to parse

    ### Return ###
    A set of the multi player games as specified in the gamelist
    """
    info("Parsing the gamelist")
    multi_player_games = set()
    tree = xml.parse(gamelist)
    root = tree.getroot()
    for game in root.findall("game"):
        players = game.findtext("players")
        name = game.findtext("name")
        debug(xml.tostring(game))
        if not players:
            debug(f"{name} had no players info")
        elif players == "1":
            debug(f"{name} was a single player game")
        else:
            debug(f"{name} added to multi player list")
            game_path = path.abspath(
                path.join(source_dir, game.findtext("path")))
            multi_player_games.add(game_path)
    return multi_player_games


def remove_existing(destination: PathLike):
    """
    Remove all existing symlinks from a destination directory

    ### Args ###
    - `destination`: The destination directory to remove symlinks from
    """
    info("Removing existing symlinks")
    for entry in os.scandir(destination):
        if entry.is_symlink and not entry.is_dir:
            debug(f"Removing: {entry.name}")
            os.remove(entry)


def link_games(games: Iterable[PathLike], destination: PathLike):
    """
    Create symlinks for a collection of given game paths to a destination directory

    ### Args ###
    - `games`: The games to create symlinks for
    - `destination`: The destination directory to create symlinks
    """
    info("Creating new symlinks")
    for game in games:
        new_link = path.abspath(path.join(destination, path.basename(game)))
        debug(f"Creating symlink: {new_link}")
        try:
            os.symlink(game, new_link)
        except FileExistsError:
            warning(f"{new_link} exists, skipping")


def parse_args() -> argparse.Namespace:
    """
    Parse the command line arguments for this script

    ### Return ###
    The parsed arguments
    """
    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("sys", help="The system to create symlinks for",
                        choices=["gb", "gbc"])
    parser.add_argument("--remove",
                        action="store_true",
                        help="Remove existing symlinks in the destination")
    parser.add_argument("--roms-dir",
                        help=f"The parent directory for the roms location. \
                            Defaults to `{DEFAULT_ROMS_DIR}`",
                        default=DEFAULT_ROMS_DIR)
    parser.add_argument("--gamelist",
                        help=f"Name of the gamelist file to parse when \
                            determining player count. Defaults to \
                            `{DEFAULT_GAMELIST_NAME}`",
                        default=DEFAULT_GAMELIST_NAME)
    parser.add_argument("--log",
                        choices=["debug", "info", "warn", "error"],
                        default="warn",
                        help="Logging level, defaults to WARN")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=getattr(logging, args.log.upper(), None),
                        force=True)
    print(logging.root.getEffectiveLevel())
    main()
